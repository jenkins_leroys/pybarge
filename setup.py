import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pybarge",
    version="0.0.1",
    author="Roman Klimov || Nikita Shishkin",
    author_email="klimov.roma04@yandex.ru || nik.asiimov@mail.ru",
    description="project manager for Python",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jenkins_leroys/pybarge",
    project_urls={
        "Bug Tracker": "https://gitlab.com/jenkins_leroys/pybarge/-/incidents",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: GNU/Linux",
    ],
    package_dir={"": "src"},
    # scripts=["src/pybarge/__main__.py"],
    entry_points={"console_scripts": ["pybarge=pybarge:main"]},
    packages=setuptools.find_packages(where="src"),
    install_requires=["pydantic>=1.8.2", "toml>=0.10.2", "typer>=0.4.0", "requests>=2.26.0", "progress>=1.6"],
    python_requires=">=3.9",
)
