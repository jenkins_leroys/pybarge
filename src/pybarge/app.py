from typer import Typer
from pybarge.config import Config
from os import getcwd
from os.path import dirname, abspath
from sys import version_info, platform
from pathlib import PurePath, PureWindowsPath


if platform in ("linux", "linux2", "darwin"):
    MyPath = PurePath
else:
    MyPath = PureWindowsPath

pybarge_instance: Typer = Typer()
pybarge_config = Config(
    pybarge_root=dirname(abspath(__file__)),
    project_root=getcwd(),
    running_os=platform,
    python_version=version_info
)
