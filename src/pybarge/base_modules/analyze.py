from pybarge.config import PybargeConfig
from subprocess import call
from pybarge.app import pybarge_instance


class AnalyzeConfig(PybargeConfig):
    analyzecmd: str = "pylint main.py"


@pybarge_instance.command()
def analyze():
    run_section = AnalyzeConfig("test")

    call([*run_section.analyzecmd.split()])
