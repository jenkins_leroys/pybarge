from os import popen
from pybarge.app import pybarge_instance, pybarge_config, MyPath
from venv import create as venv_create


@pybarge_instance.command()
def create(name: str) -> None:
    current_path = MyPath(pybarge_config.project_root)
    pybarge_root = MyPath(pybarge_config.pybarge_root)

    popen(f"mkdir -p {current_path}/{name}")
    popen(f"cp -ra {pybarge_root}/template/. {current_path}/{name}")
    popen(f"git init {current_path}/{name} --initial-branch=main")

    venv_create(str(current_path/name/"pybarge_venv"))
