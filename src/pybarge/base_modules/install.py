# from os import popen
from typing import List
# import pybarge
from pybarge.app import pybarge_instance
import sys
from pybarge.base_modules.__pkg_utils import get_package_info
from pathlib import Path
# import json
import requests
from progress.bar import Bar
import math
import shutil
import subprocess


def get_print_pkg_name_line(pkg_name: str) -> str:
    cli_size = shutil.get_terminal_size((80, 20)).columns
    cli_size -= 12  # len("Downloading ")
    cli_size -= 36  # len(" |################################| ")
    cli_size -= 5  # len(" 100%")
    cli_size -= 10
    minimal_pkg_name_len = len(pkg_name.split("-")[0])
    if cli_size >= len(pkg_name):
        spaces_to_add = cli_size - len(pkg_name)
    elif cli_size < len(pkg_name) and cli_size >= minimal_pkg_name_len:
        spaces_to_add = cli_size - minimal_pkg_name_len
        pkg_name = pkg_name[:minimal_pkg_name_len]
    else:
        spaces_to_add = 0

    return f"   >>> Dowloading {pkg_name} {'-'*(spaces_to_add-4)} eta %(eta)ds"


def get_pretty_line(left: str, middle: str, right: str, fill: str, c: float) -> str:
    cli_size = shutil.get_terminal_size((80, 20)).columns
    cli_size -= len(right)
    cli_size -= len(left)
    cli_size -= len(middle)
    fill_len = max(cli_size-4, 0)
    fill_before = int(fill_len*c)
    fill_after = fill_len - fill_before
    return f"{left} {fill*fill_before} {middle} {fill*fill_after} {right}"


@pybarge_instance.command()
def install(names: List[str]) -> None:
    if not names:
        ...  # raise name error (name must be specified)
        print("no names provided")

    packages = {}

    for name in names:
        if not name:
            print("bad name", name)
            break
            ...  # raise name error (name must be specified)

        version = ""
        separator = ">" if ">" in name else ("<" if "<" in name else ("=" if "=" in name else ""))
        separator += "=" if "=" in name else ""
        if len(separator) == 2:
            name, version = name.split(separator)[1]

        package_info = get_package_info(name, version)

        if not package_info:
            print("bad name or version", name, version)
            continue
            ...  # raise name | version error (bad name|version)
        packages[name] = package_info

    print("Dowloading specified packages:")

    for name, pkg in packages.items():
        url: str = pkg["url"]

        filename = Path(sys.prefix) / "tmp"
        filename.mkdir(parents=True, exist_ok=True)
        filename = filename / url.split("/")[-1]
        chunk_size = 1024

        packages[name]["filename"] = filename

        bar = Bar(
            get_print_pkg_name_line(url.split('/')[-1]),
            max=math.ceil(pkg["size"]/chunk_size),
            suffix='%(percent)d%%',
        )

        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with open(filename, "wb") as f:
                for chunk in r.iter_content(chunk_size=chunk_size):
                    f.write(chunk)
                    bar.next()
        print()

    not_ok = []
    for name, pkg in packages.items():
        print(f"Instaling package `{name}`")
        asd = subprocess.Popen(
            [sys.executable, "-m", "pip", "install", "--force-reinstall", pkg["filename"]],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )
        req_name = ""
        while True:
            outs = asd.stdout.readline().decode()
            errs = asd.stderr.read().decode()

            if "ERROR" in errs:
                print(errs)
                print("Aborting!")
                asd.kill()
                not_ok.append(name)
                break
            elif "WARNING" in errs:
                pass
            elif errs:
                print("errs:", errs)

            if not outs:
                break
            words = outs.replace("\n", "").split()
            if words[0] == "Processing":
                pass
            elif words[0] == "Collecting":
                req_name = words[1]
            elif words[0] + " " + words[1] == "Using cached":
                print(get_pretty_line(f"Found requirenment: `{req_name}`", "using cached", f"`{words[2]}`", "-", 4/5))
                # print(f"Found requirenment: `{req_name}` -- using cached `{words[2]}`")
            elif "Installing collected packages" in outs:
                print("Process with installation:")
            elif "Attempting uninstall" in outs \
                    or "Found existing installation" in outs \
                    or "Uninstalling" in outs \
                    or "Successfully uninstalled" in outs \
                    or "Using legacy" in outs:
                pass
            elif "Successfully installed" in outs:
                print(f"Package `{name}` was successfully installed")
            elif "Running setup.py install" in outs:
                print(outs.replace("\n", ""))
            else:
                print("outs:", outs)

    if not_ok:
        print("WARNING: The folowing packages was not installed due to fatal errors:\n", f"`{'` `'.join(not_ok)}`")
