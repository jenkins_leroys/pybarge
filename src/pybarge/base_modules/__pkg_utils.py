import xmlrpc.client
from time import sleep


client = xmlrpc.client.ServerProxy('https://pypi.org/pypi')


def get_package_info(name: str, version: str = ''):
    if not version:
        version = client.package_releases(name)
        if not version:
            return {}

        version = version[0]

        sleep(1)

    package_url = client.release_urls(name, version)
    if not package_url:
        return {}

    index = 0
    for info in package_url:
        if info["packagetype"] == 'bdist_wheel':
            break
        index += 1
    else:
        index = 0

    package_size = package_url[index]['size']
    package_url = package_url[index]['url']
    return {
        'version': version,
        'url': package_url,
        'size': package_size
    }
