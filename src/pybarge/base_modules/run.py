from typing import Optional
from pybarge.config import PybargeConfig
from pybarge.app import pybarge_instance
# from os import getcwd
from sys import version_info
from pybarge.errors import PybargeError
from subprocess import call


class BaseConfig(PybargeConfig):
    project_name: str
    version: str
    version_of_python: str
    authors: list[str] = []


# несколько entrypoint'ов - по пложенной секции на каждый
# общий формат entrypoint (как лучше реализовать запуск проекта не через python)
class RunConfig(PybargeConfig):
    entrypoint: str = "main.py"
    interpreter: Optional[str] = "python"


def check_python_version(str_version: str):
    version = tuple(str_version.split("."))

    if int(version[0]) != version_info[0]:
        raise PybargeError(
            f"You are trying to run Python {version[0]} project with Python {version_info[0]} interpreter"
        )

    if version_info[1] < int(version[1]) or version_info[2] < int(version[2]):
        raise PybargeError(
            f"Current version of interpreter ({'.'.join(map(str, version_info[:3]))}) "
            f"is less than minimal required ({str_version})"
        )


@pybarge_instance.command()
def run():
    main_section = BaseConfig("project")
    check_python_version(main_section.version_of_python)

    run_section = RunConfig("run_groups")

    call([run_section.interpreter, *run_section.entrypoint.split()])
