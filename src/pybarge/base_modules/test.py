from pybarge.config import PybargeConfig
from subprocess import call
from pybarge.app import pybarge_instance


class TestConfig(PybargeConfig):
    testcmd: str = "python test.py"


@pybarge_instance.command()
def test():
    run_section = TestConfig("test")

    call([*run_section.testcmd.split()])
