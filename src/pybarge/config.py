from typing import Any, Union
from pybarge.errors import PybargeConfigureError, PybargeUnexpectedError
from pydantic import ValidationError, BaseModel
import os
import toml


class PybargeConfig(BaseModel):
    def __init__(self, section: str):
        config = PybargeConfig.__load_config_from_file(f"{os.getcwd()}/pybarge.toml")
        if section not in config:
            raise PybargeConfigureError(f"no section \"{section}\" found in your configuration file")

        try:
            super().__init__(**config[section])

        except ValidationError as e:
            raise PybargeConfigureError(f"Error loading section \"{section}\": [{str(e)}]") from e

        except Exception as e:
            raise PybargeUnexpectedError(f"unexpected error occured: {e}") from e

    @staticmethod
    def __load_config_from_file(filename) -> dict:
        try:
            data: dict = toml.loads(open(filename, "r", encoding="utf-8").read())

        except FileNotFoundError as e:
            raise PybargeConfigureError("No pybarge.toml configuration file found!") from e

        except toml.decoder.TomlDecodeError as e:
            raise PybargeConfigureError(f"Corrupted pybarge.toml configuration file: [{e.msg}]")

        return data


class Config():
    pybarge_root: str = None
    project_root: str = None
    running_os: str = None
    python_version: Union[tuple, Any] = None

    def __init__(self, **kwargs) -> None:
        for key, val in kwargs.items():
            if hasattr(self, key):
                setattr(self, key, val)
            # else:
            #     print(key)
