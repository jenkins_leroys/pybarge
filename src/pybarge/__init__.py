import pybarge.base_modules  # noqa
import pybarge.modules  # noqa
from pybarge.errors import PybargeError
from pybarge.app import pybarge_instance


def main():
    try:
        pybarge_instance()
    except PybargeError as e:
        print(e)
