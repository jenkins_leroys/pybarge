
class PybargeError(Exception):
    text = ""
    prefix = "some of PybargeErrors occured: "

    def __str__(self) -> str:
        return self.prefix + self.text

    def __init__(self, text: str) -> None:
        self.text = text


class PybargeConfigureError(PybargeError):
    prefix = "PybargeConfigureError occured: "


class PybargeUnexpectedError(PybargeError):
    prefix = "PybargeUnexpectedError occured: "


class PybargeLockError(PybargeError):
    prefix = "It seems, another instance of pybarge pkg maneger is alrerady runnig!\n"
    "please, consider stopping all other instances or waiting for all the operation are completed!\n"
    "If definitelly know, no other instances are run, just delete "  # file name adds as a text value


class PybargePkgError(PybargeError):
    prefix = "something wrong with your pybarge pkg configuration: "
