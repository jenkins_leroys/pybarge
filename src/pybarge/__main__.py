#!/usr/bin/env python
import pybarge.base_modules  # noqa
import pybarge.modules  # noqa
import pybarge


if __name__ == "__main__":
    pybarge.main()
