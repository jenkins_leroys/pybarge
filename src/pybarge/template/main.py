from config import Config


def main() -> None:
    cfg = Config()  # noqa
    print('Hello world')


if __name__ == "__main__":
    main()
