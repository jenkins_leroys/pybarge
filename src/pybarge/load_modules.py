from os import listdir
from pybarge.app import pybarge_config
import importlib


def load_modules(module_dir: str) -> None:
    for name in listdir(f"{pybarge_config.pybarge_root}/{module_dir}"):
        if name != "__init__.py" and ".py" in name:
            importlib.import_module(f"pybarge.{module_dir}.{name.replace('.py', '')}")
